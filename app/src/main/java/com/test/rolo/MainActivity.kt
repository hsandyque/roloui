package com.test.rolo

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.chip.Chip
import com.test.rolo.adapter.ContactsAdapter
import com.test.rolo.listener.ContactSelectListener
import com.test.rolo.model.Constant
import com.test.rolo.model.Contact
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_message.*
import kotlinx.android.synthetic.main.item_receiver.*
import kotlinx.android.synthetic.main.item_subject.*
import kotlinx.android.synthetic.main.view_new_convo.*
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity(), ContactSelectListener,
    ViewTreeObserver.OnGlobalLayoutListener {

    private var mDummyContacts: ArrayList<Contact> = arrayListOf(
        Contact("Andy", "andy@xxx.ccc", R.drawable.avatars_material_man_1),
        Contact("Alice", "alice@xxx.ccc", R.drawable.avatars_material_woman_1),
        Contact("Bill", "bill@xxx.ccc", R.drawable.avatars_material_man_2),
        Contact("Cindy", "cindy@xxx.ccc", R.drawable.avatars_material_woman_2),
        Contact("Dan", "dan@xxx.ccc", R.drawable.avatars_material_man_3),
        Contact("Eric", "eric@xxx.ccc", R.drawable.avatars_material_man_4),
        Contact("Fred", "fred@xxx.ccc", R.drawable.avatars_material_man_5),
        Contact("Helen", "helen@xxx.ccc", R.drawable.avatars_material_woman_3),
        Contact("Jerry", "jerry@xxx.ccc", R.drawable.avatars_material_man_1),
        Contact("Justin", "justin@xxx.ccc", R.drawable.avatars_material_man_3),
        Contact("Lucy", "lucy@xxx.ccc", R.drawable.avatars_material_woman_4),
        Contact("Molly", "molly@xxx.ccc", R.drawable.avatars_material_woman_5),
        Contact("Neo", "neo@xxx.ccc", R.drawable.avatars_material_man_2),
        Contact("Peter", "peter@xxx.ccc", R.drawable.avatars_material_man_3),
        Contact("roy", "roy@xxx.ccc", R.drawable.avatars_material_man_4),
        Contact("Tina", "tina@xxx.ccc", R.drawable.avatars_material_woman_1),
        Contact("Vera", "vera@xxx.ccc", R.drawable.avatars_material_woman_2),
        Contact("Will", "will@xxx.ccc", R.drawable.avatars_material_man_5),
        Contact("Wella", "will@xxx.ccc", R.drawable.avatars_material_woman_3)
    )

    private lateinit var mBottomBehavior: BottomSheetBehavior<View>
    private lateinit var mContactAdapter: ContactsAdapter
    private lateinit var decorView: View
    private var mIsChipRemove = false
    private val mWindowVisibleDisplayFrame = Rect()
    private var mLastVisibleDecorViewHeight = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mBottomBehavior = BottomSheetBehavior.from(bottomsheet_layout)
        mBottomBehavior.addBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if(newState == BottomSheetBehavior.STATE_HIDDEN) {
                    resetAllSection()
                }
            }
        })
        decorView = window.decorView
        decorView.viewTreeObserver.addOnGlobalLayoutListener(this)
        setupViews()
    }

    override fun onDestroy() {
        super.onDestroy()
        decorView.viewTreeObserver.removeOnGlobalLayoutListener(this)
    }

    override fun onSelect(contact: Contact) {
        val vChip = getChip(contact)
        vChip.setOnCloseIconClickListener {
            chip_group_vertical.removeView(vChip)
            if (chip_group_vertical.childCount == 0) {
                resetReceiverSection()
            }
            removeHorizonChip(contact)
            mContactAdapter.removeFromExcludeList(contact)
        }
        addVerticalChip(vChip)

        val hChip = getChip(contact)
        hChip.setOnCloseIconClickListener {
            mIsChipRemove = true
            chip_group_horizontal.removeView(hChip)
            if (chip_group_horizontal.childCount == 0) {
                resetReceiverSection()
            }
            removeVerticalChip(contact)
            mContactAdapter.removeFromExcludeList(contact)
        }
        addHorizonChip(hChip)

        mContactAdapter.addToExludeList(contact)
    }

    override fun onBackPressed() {
        if(bottomsheet_layout.visibility == View.VISIBLE) {
            hideBottomSheet()
        } else {
            super.onBackPressed()
        }
    }

    //For check if keyboard is hide
    override fun onGlobalLayout() {
        decorView.getWindowVisibleDisplayFrame(mWindowVisibleDisplayFrame)
        val visibleDecorViewHeight = mWindowVisibleDisplayFrame.height()
        if (mLastVisibleDecorViewHeight != 0) {
            if (mLastVisibleDecorViewHeight + 50 < visibleDecorViewHeight) {
                if (actv_receiver.hasFocus()) {
                    actv_receiver.clearFocus()
                }
                if (et_message.hasFocus()) {
                    et_message.clearFocus()
                }
                if (et_subject.hasFocus()) {
                    et_subject.clearFocus()
                }
            }
        }
        mLastVisibleDecorViewHeight = visibleDecorViewHeight
    }

    private fun setupViews() {
        setupReceiverView()
        setupSubjectView()
        setupMessageView()
        btn_new_convo.setOnClickListener{
            showBottomSheet()
        }
    }

    private fun setupReceiverView() {
        mContactAdapter = ContactsAdapter(
            this, R.layout.item_list_contact, mDummyContacts, this)
        actv_receiver.setAdapter(mContactAdapter)
        actv_receiver.threshold = 1
        actv_receiver.imeOptions = EditorInfo.IME_ACTION_GO

        actv_receiver.setOnEditorActionListener { _, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER ||
                actionId == EditorInfo.IME_ACTION_GO) {
                val inputText = actv_receiver.text.toString().replace("^\\s+","").replace("\\s+$","")
                if (inputText.isNotEmpty()) {
                    val vChip = getChip(inputText)
                    vChip.setOnCloseIconClickListener {
                        chip_group_vertical.removeView(vChip)
                        removeHorizonChip(inputText)
                        if (chip_group_vertical.childCount == 0) {
                            resetReceiverSection()
                        }
                    }
                    addVerticalChip(vChip)

                    val hChip = getChip(inputText)
                    hChip.setOnCloseIconClickListener {
                        mIsChipRemove = true
                        chip_group_horizontal.removeView(hChip)
                        removeVerticalChip(inputText)
                        if (chip_group_horizontal.childCount == 0) {
                            resetReceiverSection()
                        }
                    }
                    addHorizonChip(hChip)
                }
            }
            false
        }

        actv_receiver.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (count > 0 || chip_group_vertical.childCount > 0) {
                    tv_receiver_hint.visibility = View.VISIBLE
                    actv_receiver.hint = ""
                } else {
                    tv_receiver_hint.visibility = View.GONE
                    actv_receiver.setHint(R.string.label_receiver_hint)
                }
            }
        })

        actv_receiver.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            layout_message.visibility = if (hasFocus) {
                View.GONE
            } else {
                View.VISIBLE
            }
            val bgResId = if(hasFocus) {
                R.drawable.bg_receiver_view_focus
            } else {
                R.drawable.bg_receiver_view_default
            }
            layout_receiver.setBackgroundResource(bgResId)
            updateChipGroup(hasFocus)
        }
        layout_receiver.setOnClickListener {
            actv_receiver.requestFocus()
            showKeypad()
        }
    }

    private fun setupSubjectView() {
        et_subject.filters = arrayOf<InputFilter>(
            InputFilter.LengthFilter(Constant.MAX_SUBJECT_LENGTH)
        )

        et_subject.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val sb = StringBuilder()
                sb.append(count.toString())
                sb.append("/")
                sb.append(Constant.MAX_SUBJECT_LENGTH)
                tv_subject_count.text = sb.toString()
                if (count > 0) {
                    tv_subject_hint.visibility = View.VISIBLE
                } else {
                    tv_subject_hint.visibility = View.GONE
                }
            }
        })

        et_subject.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            layout_message.visibility = if (hasFocus) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }

    private fun setupMessageView() {
        et_message.filters = arrayOf<InputFilter>(
            InputFilter.LengthFilter(Constant.MAX_MESSAGE_LENGTH)
        )

        et_message.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var messageCount = 0
                s?.let {
                    messageCount = it.length
                }

                btn_send_message.isEnabled = messageCount > 0

                if (messageCount >= Constant.WARM_MESSAGE_LENGTH) {
                    val sb = StringBuilder()
                    sb.append(messageCount.toString())
                    sb.append("/")
                    sb.append(Constant.MAX_MESSAGE_LENGTH)
                    tv_message_count.text = sb.toString()
                    tv_message_count.visibility = View.VISIBLE
                } else {
                    tv_message_count.visibility = View.INVISIBLE
                }
            }
        })

        iv_expand.setOnClickListener {
            iv_expand.visibility = View.GONE
            layout_message.updateLayoutParams {
                height = convertDpToPixel(300F).roundToInt()
            }
        }
        et_message.clearFocus()
    }

    private fun resetAllSection() {
        //receiver section
        resetReceiverSection()

        //subject section
        et_subject.text?.clear()

        //message section
        et_message.text?.clear()
        iv_expand.visibility = View.VISIBLE
        layout_message.updateLayoutParams {
            height = convertDpToPixel(150F).roundToInt()
        }
    }

    private fun resetReceiverSection() {
        horizontal_scroll_view.visibility = View.GONE
        chip_group_vertical.visibility = View.GONE
        chip_group_vertical.removeAllViews()
        chip_group_horizontal.removeAllViews()
        actv_receiver.text.clear()
        tv_receiver_hint.visibility = View.GONE
        actv_receiver.setHint(R.string.label_receiver_hint)
    }

    private fun showBottomSheet() {
        bottomsheet_layout.visibility = View.VISIBLE
        if(mBottomBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            mBottomBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun hideBottomSheet() {
        bottomsheet_layout.visibility = View.GONE
        if(mBottomBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun convertDpToPixel(dp: Float): Float {
        val density: Float = resources.displayMetrics.density
        return dp * density
    }
    private fun showKeypad() {
        try {
            val view = currentFocus
            view?.postDelayed({
                val imm = getSystemService(
                    Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(view, InputMethodManager.RESULT_SHOWN)
            }, 200)
        } catch (e: Exception) { }
    }

    private fun getChip(contact: Contact): Chip {
        val chip = layoutInflater.inflate(R.layout.item_chip_contact, null) as Chip
        chip.isCheckable = false
        chip.isCloseIconVisible = true
        chip.text = contact.name
        chip.chipIcon = ContextCompat.getDrawable(this, contact.avatar)
        chip.tag = contact
        return chip
    }

    private fun getChip(name: String): Chip {
        val chip = layoutInflater.inflate(R.layout.item_chip_contact, null) as Chip
        chip.isCheckable = false
        chip.isCloseIconVisible = true
        chip.text = name
        chip.tag = name
        return chip
    }
    private fun removeVerticalChip(contact: Contact) {
        val childCount = chip_group_vertical.childCount
        var removeIndex = -1
        for(i in 0 until childCount) {
            val chipTag = chip_group_vertical.getChildAt(i).tag
            if(chipTag is Contact) {
                if(chipTag.name == contact.name &&
                    chipTag.email == contact.email &&
                        chipTag.avatar == contact.avatar){
                    removeIndex = i
                    break
                }
            }
        }
        if(removeIndex != -1) {
            chip_group_vertical.removeViewAt(removeIndex)
        }
    }

    private fun removeVerticalChip(name: String) {
        val childCount = chip_group_vertical.childCount
        var removeIndex = -1
        for(i in 0 until childCount) {
            val chipTag = chip_group_vertical.getChildAt(i).tag
            if(chipTag is String) {
                if(chipTag == name){
                    removeIndex = i
                    break
                }
            }
        }
        if(removeIndex != -1) {
            chip_group_vertical.removeViewAt(removeIndex)
        }
    }
    private fun removeHorizonChip(contact: Contact) {
        val childCount = chip_group_horizontal.childCount
        var removeIndex = -1
        for(i in 0 until childCount) {
            val chipTag = chip_group_horizontal.getChildAt(i).tag
            if(chipTag is Contact) {
                if(chipTag.name == contact.name &&
                    chipTag.email == contact.email &&
                    chipTag.avatar == contact.avatar){
                    removeIndex = i
                    break
                }
            }
        }
        if(removeIndex != -1) {
            chip_group_horizontal.removeViewAt(removeIndex)
        }
    }
    private fun removeHorizonChip(name: String) {
        val childCount = chip_group_horizontal.childCount
        var removeIndex = -1
        for(i in 0 until childCount) {
            val chipTag = chip_group_horizontal.getChildAt(i).tag
            if(chipTag is String) {
                if(chipTag == name){
                    removeIndex = i
                    break
                }
            }
        }
        if(removeIndex != -1) {
            chip_group_horizontal.removeViewAt(removeIndex)
        }
    }

    private fun addVerticalChip(chip: Chip) {
        chip_group_vertical.addView(chip)
        chip_group_vertical.visibility = View.VISIBLE
        actv_receiver.setText("")
        actv_receiver.text.clear()
    }

    private fun addHorizonChip(chip: Chip) {
        chip_group_horizontal.addView(chip)
    }

    private fun updateChipGroup(hasFocus: Boolean) {
        if (hasFocus) {
            horizontal_scroll_view.visibility = View.GONE
            chip_group_vertical.visibility =  if(chip_group_vertical.childCount > 0 ) {
                View.VISIBLE
            } else {
                View.GONE
            }
        } else {
            chip_group_vertical.visibility = View.GONE
            horizontal_scroll_view.visibility = if(chip_group_horizontal.childCount > 0) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }


}
