package com.test.rolo.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import androidx.annotation.LayoutRes
import com.test.rolo.R
import com.test.rolo.listener.ContactSelectListener
import com.test.rolo.model.Contact
import kotlinx.android.synthetic.main.item_list_contact.view.*
import kotlin.collections.ArrayList

class ContactsAdapter(
    context: Context, @LayoutRes resource: Int,
    private val contactList: ArrayList<Contact>,
    private val listener: ContactSelectListener?
) :
    ArrayAdapter<Contact>(context, resource), Filterable {

    private var mFilterList = ArrayList<Contact>()
    private var mExcludeList = ArrayList<Contact>()
    private val mColorCode: ArrayList<String> = ArrayList(
        listOf(
            "#EF9A9A", "#F48FB1", "#CE93D8", "#B39DDB", "#9FA8DA",
            "#90CAF9", "#81D4FA", "#80DEEA", "#80CBC4", "#A5D6A7",
            "#C5E1A5", "#E6EE9C", "#FFF59D", "#FFE082", "#FFCC80"
        ).shuffled()
    )
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: createView(position, parent)
    }

    private fun createView(position: Int, parent: ViewGroup?): View {
        val contact = mFilterList[position]

        val view = LayoutInflater.from(context).inflate(R.layout.item_list_contact, parent, false)
        view.tv_name.text = contact.name
        view.tv_email.text = contact.email
        view.iv_avatar.setImageResource(contact.avatar)
        view.cv_avatar.setCardBackgroundColor(Color.parseColor(mColorCode[position]))
        view.setOnClickListener {
            listener?.onSelect(contact)
        }
        return view
    }

    override fun getCount() = mFilterList.size

    override fun getItem(position: Int) = mFilterList[position]

    override fun getFilter(): Filter {
        return return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()

                constraint?.let {
                    val list = getAutocomplete(it.toString())
                    results.values = list
                    results.count = list.size
                } ?:run {
                    val allList = arrayListOf<Contact>()
                    for (contact in contactList) {
                        if (!mExcludeList.contains(contact)) {
                            allList.add(contact)
                        }
                    }
                    results.values = allList
                    results.count = allList.size
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                mFilterList = arrayListOf()
                results?.let {
                    it.values?.let { list ->
                        mFilterList.addAll(list as ArrayList<Contact>)
                    }
                }
                notifyDataSetChanged()
            }

        }
    }

    private fun getAutocomplete(filter: String): ArrayList<Contact> {
        val filterLowerCase = filter.toLowerCase()
        val results = arrayListOf<Contact>()
        if (filter.isEmpty()) {
            results.addAll(contactList)
        } else {
            for (contact in contactList) {
                if((contact.name.toLowerCase().contains(filterLowerCase) ||
                     contact.email.toLowerCase().contains(filterLowerCase)) &&
                    !mExcludeList.contains(contact)) {
                    results.add(contact)
                }
            }
        }
        return results
    }

    fun addToExludeList(item: Contact) {
        mExcludeList.add(item)
        mFilterList.remove(item)
        notifyDataSetChanged()
    }

    fun removeFromExcludeList(item: Contact) {
        mExcludeList.remove(item)
        mFilterList.add(item)
        val sortedList = mFilterList.sortedWith(compareBy { it.name })
        mFilterList.clear()
        mFilterList.addAll(sortedList)
        notifyDataSetChanged()
    }

    fun getExcludeList():ArrayList<Contact> {
        return mExcludeList
    }
}