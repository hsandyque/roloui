package com.test.rolo.listener

import com.test.rolo.model.Contact

interface ContactSelectListener {
    fun onSelect(contact: Contact)
}