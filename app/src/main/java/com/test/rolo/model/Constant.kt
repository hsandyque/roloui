package com.test.rolo.model

class Constant {
    companion object {
        const val MAX_SUBJECT_LENGTH = 50
        const val MAX_MESSAGE_LENGTH = 100
        const val WARM_MESSAGE_LENGTH = 90
    }
}