package com.test.rolo.model

import androidx.annotation.DrawableRes

data class Contact (val name:String, val email: String, @DrawableRes val avatar: Int)